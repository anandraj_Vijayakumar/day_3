package fileOperations;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class AppendFile {
	public static void write(String s, File f) throws IOException {
		 FileWriter fw = new FileWriter(f, true);
		 fw.write(s);
		 fw.close();
	}

    public static void main(String[] args) {
    	try {
    		File f = new File("/Users/KC tech/Desktop/test.txt");
    		write("Text two" , f);
    	}
    	catch(IOException e) {
    		
    	}
    }

}
