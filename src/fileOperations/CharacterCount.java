package fileOperations;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class CharacterCount {
	public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader("/Users/KC tech/Desktop/test.txt"));
           int ch;
           Scanner input = new Scanner(System.in);
           System.out.println("Enter a character: ");
           char charToSearch = input.next().charAt(0);
           int counter=0;
           while((ch=reader.read()) != -1) {
               if(charToSearch == (char)ch) {
                   counter++;
               }
           };
           reader.close();
           input.close();
           System.out.println(charToSearch+" occurs " + counter+ " times");

   }

}
